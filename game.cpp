#include "../LGH/app.h"

void Init();
void Reset();
void ResetBall();
void CheckScore();
void Update();
void Render();
void KeyDown();
void KeyUp();
void FingerDown();
void FingerUp();
void FingerMotion();

App app;

#define PADDLE_WIDTH 20
#define PADDLE_HEIGHT 130
#define BALL_SIZE 20
#define MAX_WINS 5

enum GameState
{
    GS_MENU = 0,
    GS_GAME = 1,
    GS_GAMEOVER = 2,
    GS_PAUSE = 3
};

GameState gamestate;

TTF_Font *font_48;
Mix_Chunk *snd_hit;
Mix_Chunk *snd_wall;
Mix_Chunk *snd_score;
Texture title_text;
Texture subtitle_text;
Texture p1_score_text;
Texture p2_score_text;
const char* title_text_buf = "PONG";
const char* subtitle_text_buf = "TAP SCREEN TO START";
bool fingerdown = false;
bool p1_scored = false;

int p1_x, p1_y, p2_x, p2_y, ball_x, ball_y;
float player_speed = 20.0f;
float enemy_speed = 15.0f;
float ball_speed;
float ball_max_speed = 30.0f;
int p1_score, p2_score;
char p1_score_buf[32];
char p2_score_buf[32];
double ball_dir;

int main(int argc, char **argv)
{
    App_init(&app, "Pong");
    Init();
    App_mainloop(&app);
    App_uninit(&app);

    return 0;
}

void
Init()
{
    gamestate = GS_MENU;

    app.Update = Update;
    app.Render = Render;
    app.FingerUp = FingerUp;
    app.FingerDown = FingerDown;
    app.FingerMotion = FingerMotion;
    app.KeyDown = KeyDown;
    app.KeyUp = KeyUp;

#ifdef MOBILE
    font_48 = App_loadFont("GermaniaOne-Regular.TTF", 48);
    snd_hit = Mix_LoadWAV("hit.wav");
    snd_wall = Mix_LoadWAV("wall.wav");
    snd_score = Mix_LoadWAV("score.wav");

#else
    font_48 = App_loadFont("../../main/assets/GermaniaOne-Regular.TTF", 48);
    snd_hit = Mix_LoadWAV("../../main/assets/hit.wav");
    snd_wall = Mix_LoadWAV("../../main/assets/wall.wav");
    snd_score = Mix_LoadWAV("../../main/assets/score.wav");
#endif
    App_createText(&title_text, title_text_buf, font_48, colorWhite, 0);
    App_createText(&subtitle_text, subtitle_text_buf, font_48, colorWhite, 0);

    App_setWindowColor(&app.window, 0, 0, 0, 1);

    App_setAllVolume(15);

    Reset();
}

void
Reset()
{
    p1_score = 0;
    p2_score = 0;

    sprintf(p1_score_buf, "%d", p1_score);
    App_createText(&p1_score_text, p1_score_buf, font_48, colorWhite, 0);

    sprintf(p2_score_buf, "%d", p2_score);
    App_createText(&p2_score_text, p2_score_buf, font_48, colorWhite, 0);

    p1_x = 100;
    p1_y = app.window.viewport[3] / 2 - PADDLE_HEIGHT / 2;
    p2_x = app.window.viewport[2] - 100 - PADDLE_WIDTH;
    p2_y = app.window.viewport[3] / 2 - PADDLE_HEIGHT / 2;

    ResetBall();
}

void
ResetBall()
{
    ball_x = app.window.viewport[2] / 2 - BALL_SIZE / 2;
    ball_y = app.window.viewport[3] / 2 - BALL_SIZE / 2;

    if (p1_scored)
        ball_dir = 205 + rand() % 50;
    else
        ball_dir = 25 + rand() % 50;
    ball_speed = 20.0f;
}

void
CheckScore()
{
    Mix_PlayChannel(2, snd_score, 0);
    if (p1_score >= MAX_WINS)
    {
        App_createText(&subtitle_text, "VICTORY", font_48, colorGreen, 0);
    }
    else if (p2_score >= MAX_WINS)
    {
        App_createText(&subtitle_text, "DEFEAT", font_48, colorRed, 0);
    }
    else
        return;

    gamestate = GS_GAMEOVER;
}

void
Render()
{
    Draw_rectangle(p1_x, p1_y, PADDLE_WIDTH, PADDLE_HEIGHT);
    Draw_rectangle(p2_x, p2_y, PADDLE_WIDTH, PADDLE_HEIGHT);

    switch (gamestate)
    {
        case GS_MENU:
        {
            Draw_sprite(&title_text,
                app.window.viewport[2] / 2 - title_text.width / 2, 200, 0, 1.0f, 1.0f);

            Draw_sprite(&subtitle_text,
                app.window.viewport[2] / 2 - subtitle_text.width / 2,
                app.window.viewport[3] / 2,
                0, 1.0f, 1.0f);
        }break;
        case GS_GAME:
        case GS_PAUSE:
        {
            for (int i = 0; i < 32; ++i)
                Draw_rectangle(app.window.viewport[2] / 2 - 1, 64 * i + 8, 2, 24, 255, 255, 255, 172);

            Draw_rectangle(ball_x, ball_y, BALL_SIZE, BALL_SIZE);

            Draw_sprite(&p1_score_text,
                300 - p1_score_text.width / 2, 64, 0, 1.0f, 1.0f);

            Draw_sprite(&p2_score_text,
                app.window.viewport[2] - 300  - p2_score_text.width / 2, 64, 0, 1.0f, 1.0f);
        }break;
        case GS_GAMEOVER:
        {
            Draw_sprite(&subtitle_text,
                app.window.viewport[2] / 2 - subtitle_text.width / 2,
                app.window.viewport[3] / 2,
                0, 1.0f, 1.0f);

            Draw_sprite(&p1_score_text,
                300 - p1_score_text.width / 2, 64, 0, 1.0f, 1.0f);

            Draw_sprite(&p2_score_text,
                app.window.viewport[2] - 300  - p2_score_text.width / 2, 64, 0, 1.0f, 1.0f);
        }break;
    }
}

void
Update()
{
    switch (gamestate)
    {
        case GS_GAME:
        {
            if (fingerdown)
            {
                p1_y = (int)(approach((float)app.touchInputs[app.finger_index].position.y - PADDLE_HEIGHT / 2,
                    (float)p1_y, player_speed));

                if (p1_y < 0)
                    p1_y = 0;

                if (p1_y > app.window.viewport[3] - PADDLE_HEIGHT)
                    p1_y = app.window.viewport[3] - PADDLE_HEIGHT;
            }

            p2_y = (int)(approach((float)ball_y - PADDLE_HEIGHT / 2, (float)p2_y, enemy_speed));

            if (p2_y < 0)
                p2_y = 0;

            if (p2_y > app.window.viewport[3] - PADDLE_HEIGHT)
                p2_y = app.window.viewport[3] - PADDLE_HEIGHT;

            ball_x += (int)(cos(ball_dir * RADIAN) * ball_speed);
            ball_y += (int)(sin(ball_dir * RADIAN) * ball_speed);

            if (ball_y < 0)
            {
                ball_y = 0;
                ball_dir = 360 - ball_dir;
                Mix_PlayChannel(2, snd_wall, 0);
            }
            if (ball_y >= app.window.viewport[3] - BALL_SIZE)
            {
                ball_y = app.window.viewport[3] - BALL_SIZE;
                ball_dir = 360 - ball_dir;
                Mix_PlayChannel(2, snd_wall, 0);
            }

            uint p1_rect[4] = {(uint)p1_x, (uint)p1_y, PADDLE_WIDTH, PADDLE_HEIGHT };
            uint p2_rect[4] = {(uint)p2_x, (uint)p2_y, PADDLE_WIDTH, PADDLE_HEIGHT };
            uint ball_rect[4] = {(uint)ball_x, (uint)ball_y, BALL_SIZE, BALL_SIZE };

            if (AABB(p1_rect, ball_rect))
            {
                Mix_PlayChannel(0, snd_hit, 0);
                ball_dir = PADDLE_HEIGHT / 2 - (ball_y - p1_y);
                ball_dir = 360 - ball_dir;
                if (ball_speed < ball_max_speed)
                    ball_speed += 2.0f;
            }
            if (AABB(p2_rect, ball_rect))
            {
                Mix_PlayChannel(1, snd_hit, 0);
                ball_dir = 180 + (PADDLE_HEIGHT / 2 - (ball_y - p2_y));
                if (ball_speed < ball_max_speed)
                    ball_speed += 2.0f;
            }

            if (ball_x < 0)
            {
                p2_score++;
                sprintf(p2_score_buf, "%d", p2_score);
                App_createText(&p2_score_text, p2_score_buf, font_48, colorWhite, 0);
                p1_scored = false;
                ResetBall();
                CheckScore();
            }
            else if (ball_x > app.window.viewport[2] - BALL_SIZE)
            {
                p1_score++;
                sprintf(p1_score_buf, "%d", p1_score);
                App_createText(&p1_score_text, p1_score_buf, font_48, colorWhite, 0);
                p1_scored = true;
                ResetBall();
                CheckScore();
            }
        }break;
    }
}

void
KeyDown()
{
    if (app.keyboardInput == SDLK_ESCAPE)
        App_shutDown(&app);
}

void
KeyUp()
{
}

void
FingerUp()
{
    if (gamestate == GS_GAME)
        fingerdown = false;
}

void
FingerDown()
{
    if (gamestate == GS_MENU)
        gamestate = GS_GAME;
    else if (gamestate == GS_GAMEOVER)
    {
        App_createText(&subtitle_text, "TAP SCREEN TO START", font_48, colorWhite, 0);
        gamestate = GS_MENU;
        Reset();
    }
    else if (gamestate == GS_GAME)
        fingerdown = true;
}

void
FingerMotion()
{
}
